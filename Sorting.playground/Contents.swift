//: Playground - noun: a place where people can play

import UIKit


func createRandomArray() -> [Int] {
    var numbers = [Int]()
    for _ in 0...10 {
        let number = Int(arc4random_uniform(100))
        numbers.append(number)
    }
    return numbers
}

//1. Bubble Sorting

func bubbleSorting() {
    var numbers = createRandomArray()
    var swapped: Bool = true
    repeat {
        swapped = false
        for var i = 1; i < numbers.count; i++ {
            if numbers[i-1] > numbers[i] {
                let b = numbers[i]
                numbers[i] = numbers[i-1]
                numbers[i-1]=b
                swapped = true
            }
        }
    } while(swapped)
    print(numbers)
}
